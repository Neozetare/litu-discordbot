# -*- coding: utf-8 -*-

import base64 as b64
from math import sqrt, ceil

from PIL import Image

ALPHA_VALUE = 1

B64_TO_RGBA = {
    'A': (0, 0, 0, ALPHA_VALUE),
    'B': (0, 0, 85, ALPHA_VALUE),
    'C': (0, 0, 170, ALPHA_VALUE),
    'D': (0, 0, 255, ALPHA_VALUE),
    'E': (0, 85, 0, ALPHA_VALUE),
    'F': (0, 85, 85, ALPHA_VALUE),
    'G': (0, 85, 170, ALPHA_VALUE),
    'H': (0, 85, 255, ALPHA_VALUE),
    'I': (0, 170, 0, ALPHA_VALUE),
    'J': (0, 170, 85, ALPHA_VALUE),
    'K': (0, 170, 170, ALPHA_VALUE),
    'L': (0, 170, 255, ALPHA_VALUE),
    'M': (0, 255, 0, ALPHA_VALUE),
    'N': (0, 255, 85, ALPHA_VALUE),
    'O': (0, 255, 170, ALPHA_VALUE),
    'P': (0, 255, 255, ALPHA_VALUE),
    'Q': (85, 0, 0, ALPHA_VALUE),
    'R': (85, 0, 85, ALPHA_VALUE),
    'S': (85, 0, 170, ALPHA_VALUE),
    'T': (85, 0, 255, ALPHA_VALUE),
    'U': (85, 85, 0, ALPHA_VALUE),
    'V': (85, 85, 85, ALPHA_VALUE),
    'W': (85, 85, 170, ALPHA_VALUE),
    'X': (85, 85, 255, ALPHA_VALUE),
    'Y': (85, 170, 0, ALPHA_VALUE),
    'Z': (85, 170, 85, ALPHA_VALUE),
    'a': (85, 170, 170, ALPHA_VALUE),
    'b': (85, 170, 255, ALPHA_VALUE),
    'c': (85, 255, 0, ALPHA_VALUE),
    'd': (85, 255, 85, ALPHA_VALUE),
    'e': (85, 255, 170, ALPHA_VALUE),
    'f': (85, 255, 255, ALPHA_VALUE),
    'g': (170, 0, 0, ALPHA_VALUE),
    'h': (170, 0, 85, ALPHA_VALUE),
    'i': (170, 0, 170, ALPHA_VALUE),
    'j': (170, 0, 255, ALPHA_VALUE),
    'k': (170, 85, 0, ALPHA_VALUE),
    'l': (170, 85, 85, ALPHA_VALUE),
    'm': (170, 85, 170, ALPHA_VALUE),
    'n': (170, 85, 255, ALPHA_VALUE),
    'o': (170, 170, 0, ALPHA_VALUE),
    'p': (170, 170, 85, ALPHA_VALUE),
    'q': (170, 170, 170, ALPHA_VALUE),
    'r': (170, 170, 255, ALPHA_VALUE),
    's': (170, 255, 0, ALPHA_VALUE),
    't': (170, 255, 85, ALPHA_VALUE),
    'u': (170, 255, 170, ALPHA_VALUE),
    'v': (170, 255, 255, ALPHA_VALUE),
    'w': (255, 0, 0, ALPHA_VALUE),
    'x': (255, 0, 85, ALPHA_VALUE),
    'y': (255, 0, 170, ALPHA_VALUE),
    'z': (255, 0, 255, ALPHA_VALUE),
    '0': (255, 85, 0, ALPHA_VALUE),
    '1': (255, 85, 85, ALPHA_VALUE),
    '2': (255, 85, 170, ALPHA_VALUE),
    '3': (255, 85, 255, ALPHA_VALUE),
    '4': (255, 170, 0, ALPHA_VALUE),
    '5': (255, 170, 85, ALPHA_VALUE),
    '6': (255, 170, 170, ALPHA_VALUE),
    '7': (255, 170, 255, ALPHA_VALUE),
    '8': (255, 255, 0, ALPHA_VALUE),
    '9': (255, 255, 85, ALPHA_VALUE),
    '+': (255, 255, 170, ALPHA_VALUE),
    '/': (255, 255, 255, ALPHA_VALUE),
    '=': (0, 0, 0, 2)
}

RGBA_TO_B64 = {v: k for k, v in B64_TO_RGBA.items()}


def encode_tuples(source):
    """Encode base64 <source> to a list of RGBA tuples

    args:
        source: bytes or str representing a base64 object
    returns:
        encoded source as list of 4-tuples of int between 0 and 255
    """
    try:
        b = b64.b64encode(b64.b64decode(source))
    except Exception as e:
        raise e
    out = []
    for c in str(b)[2:-1]:
        out.append(B64_TO_RGBA[c])
    return out


def decode_tuples(source):
    """Decode list of RGBA tuples source> to base64

    args:
        source: list of 4-tuples of int between 0 and 255
    returns:
        encoded source as base64 bytes
    """
    s = ''
    for rgba in source:
        if rgba == (0, 0, 0, 0):
            break
        s += RGBA_TO_B64[rgba]
    return b64.b64encode(b64.b64decode(s))


def encode_image(source, size=None):
    """Encode base64 <source> to a RGBA PIL.Image

    args:
        source: bytes or str representing a base64 object
        size (=None): 2-tuple, containing (width, height) in pixels
            if size is None, the image will be the tiniest square that
            can fit source
    returns:
        encoded source as RGBA PIL.Image
    """
    try:
        tuples = encode_tuples(source)
    except Exception as e:
        raise e

    if size is not None:
        try:
            size = (int(size[0]), int(size[1]))
        except TypeError:
            raise TypeError('size must be a 2-tuple, containing (width, height) in pixels')

        if size[0] <= 0 or size[1] <= 0 or \
                size[0] * size[1] <= len(tuples):
            raise ValueError(f"size should be able to contain min {len(tuples)} pixels to fit source")
    else:
        size = (ceil(sqrt(len(tuples))),)
        size *= 2

    im = Image.new('RGBA', size, color=(0, 0, 0, 0))
    im.putdata(list(tuples))

    return im


def decode_image(source):
    """Decode RGBA PIL.Image <source> to base64

    args:
        source: RGBA PIL.Image
    returns:
        decoded source base64 bytes
    """
    # noinspection PyBroadException
    try:
        im = Image.open(source)
    except Exception:
        im = source

    return decode_tuples(im.getdata())


# s = "warning t=\"food death\" s=\"THE MUFFIN SONG (asdfmovie)\"\n\"I'm a muffin!\nAnd it's muffin time!\nWho wants " \
#     "a muffin?\nPlease I just wanna die! "
# im = encode_image(b64.b64encode(s.encode('utf-8')), (128, 128))
# print(s == b64.b64decode(decode_image(im)).decode('utf-8'))
# im.show()
