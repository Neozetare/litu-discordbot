# -*- coding: utf-8 -*-

import base64
import datetime
import io
import json
import math
import platform
import random
import re
import shlex
import sys
import traceback
import urllib
from collections import namedtuple, OrderedDict
from enum import Enum
from typing import List

import requests
from PIL import Image

import b64rgba
import discord


class Litu(discord.Client):
    """A Discord Util bot"""

    CMD_SOFTERR_EMOJI = '❌'
    CMD_HARDERR_EMOJI = '☠'

    BASE_COLOR = discord.Colour(0xf1c18b)

    RE_MSG_URL = \
        re.compile(r'^(?:https://)?discordapp.com/channels/(?:[0-9]{18})/(?P<chn>[0-9]{18})/(?P<msg>[0-9]{18})/?$')

    DIGIT_TO_WORD = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                     5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine'}

    DISCORD_COLORS = {k.replace('_', ''): getattr(discord.Colour, k)() for k, v in vars(discord.Colour).items()
                      if isinstance(v, classmethod) and not k.startswith('from_') and k != 'default'}
    # noinspection SpellCheckingInspection
    CSS3_COLORS = {
        'aliceblue': discord.Colour(0xf0f8ff), 'antiquewhite': discord.Colour(0xfaebd7),
        'aqua': discord.Colour(0x00ffff), 'aquamarine': discord.Colour(0x7fffd4), 'azure': discord.Colour(0xf0ffff),
        'beige': discord.Colour(0xf5f5dc), 'bisque': discord.Colour(0xffe4c4), 'black': discord.Colour(0x000000),
        'blanchedalmond': discord.Colour(0xffebcd), 'blue': discord.Colour(0x0000ff),
        'blueviolet': discord.Colour(0x8a2be2), 'brown': discord.Colour(0xa52a2a),
        'burlywood': discord.Colour(0xdeb887), 'cadetblue': discord.Colour(0x5f9ea0),
        'chartreuse': discord.Colour(0x7fff00), 'chocolate': discord.Colour(0xd2691e),
        'coral': discord.Colour(0xff7f50), 'cornflowerblue': discord.Colour(0x6495ed),
        'cornsilk': discord.Colour(0xfff8dc), 'crimson': discord.Colour(0xdc143c), 'cyan': discord.Colour(0x00ffff),
        'darkblue': discord.Colour(0x00008b), 'darkcyan': discord.Colour(0x008b8b),
        'darkgoldenrod': discord.Colour(0xb8860b), 'darkgray': discord.Colour(0xa9a9a9),
        'darkgrey': discord.Colour(0xa9a9a9), 'darkgreen': discord.Colour(0x006400),
        'darkkhaki': discord.Colour(0xbdb76b), 'darkmagenta': discord.Colour(0x8b008b),
        'darkolivegreen': discord.Colour(0x556b2f), 'darkorange': discord.Colour(0xff8c00),
        'darkorchid': discord.Colour(0x9932cc), 'darkred': discord.Colour(0x8b0000),
        'darksalmon': discord.Colour(0xe9967a), 'darkseagreen': discord.Colour(0x8fbc8f),
        'darkslateblue': discord.Colour(0x483d8b), 'darkslategray': discord.Colour(0x2f4f4f),
        'darkslategrey': discord.Colour(0x2f4f4f), 'darkturquoise': discord.Colour(0x00ced1),
        'darkviolet': discord.Colour(0x9400d3), 'deeppink': discord.Colour(0xff1493),
        'deepskyblue': discord.Colour(0x00bfff), 'dimgray': discord.Colour(0x696969),
        'dimgrey': discord.Colour(0x696969), 'dodgerblue': discord.Colour(0x1e90ff),
        'firebrick': discord.Colour(0xb22222), 'floralwhite': discord.Colour(0xfffaf0),
        'forestgreen': discord.Colour(0x228b22), 'fuchsia': discord.Colour(0xff00ff),
        'gainsboro': discord.Colour(0xdcdcdc), 'ghostwhite': discord.Colour(0xf8f8ff),
        'gold': discord.Colour(0xffd700), 'goldenrod': discord.Colour(0xdaa520),
        'gray': discord.Colour(0x808080), 'grey': discord.Colour(0x808080),
        'green': discord.Colour(0x008000), 'greenyellow': discord.Colour(0xadff2f),
        'honeydew': discord.Colour(0xf0fff0), 'hotpink': discord.Colour(0xff69b4),
        'indianred ': discord.Colour(0xcd5c5c), 'indigo ': discord.Colour(0x4b0082), 'ivory': discord.Colour(0xfffff0),
        'khaki': discord.Colour(0xf0e68c), 'lavender': discord.Colour(0xe6e6fa),
        'lavenderblush': discord.Colour(0xfff0f5), 'lawngreen': discord.Colour(0x7cfc00),
        'lemonchiffon': discord.Colour(0xfffacd), 'lightblue': discord.Colour(0xadd8e6),
        'lightcoral': discord.Colour(0xf08080), 'lightcyan': discord.Colour(0xe0ffff),
        'lightgoldenrodyellow': discord.Colour(0xfafad2), 'lightgray': discord.Colour(0xd3d3d3),
        'lightgrey': discord.Colour(0xd3d3d3), 'lightgreen': discord.Colour(0x90ee90),
        'lightpink': discord.Colour(0xffb6c1), 'lightsalmon': discord.Colour(0xffa07a),
        'lightseagreen': discord.Colour(0x20b2aa), 'lightskyblue': discord.Colour(0x87cefa),
        'lightslategray': discord.Colour(0x778899), 'lightslategrey': discord.Colour(0x778899),
        'lightsteelblue': discord.Colour(0xb0c4de), 'lightyellow': discord.Colour(0xffffe0),
        'lime': discord.Colour(0x00ff00), 'limegreen': discord.Colour(0x32cd32), 'linen': discord.Colour(0xfaf0e6),
        'magenta': discord.Colour(0xff00ff), 'maroon': discord.Colour(0x800000),
        'mediumaquamarine': discord.Colour(0x66cdaa), 'mediumblue': discord.Colour(0x0000cd),
        'mediumorchid': discord.Colour(0xba55d3), 'mediumpurple': discord.Colour(0x9370db),
        'mediumseagreen': discord.Colour(0x3cb371), 'mediumslateblue': discord.Colour(0x7b68ee),
        'mediumspringgreen': discord.Colour(0x00fa9a), 'mediumturquoise': discord.Colour(0x48d1cc),
        'mediumvioletred': discord.Colour(0xc71585), 'midnightblue': discord.Colour(0x191970),
        'mintcream': discord.Colour(0xf5fffa), 'mistyrose': discord.Colour(0xffe4e1),
        'moccasin': discord.Colour(0xffe4b5), 'navajowhite': discord.Colour(0xffdead),
        'navy': discord.Colour(0x000080), 'oldlace': discord.Colour(0xfdf5e6), 'olive': discord.Colour(0x808000),
        'olivedrab': discord.Colour(0x6b8e23), 'orange': discord.Colour(0xffa500),
        'orangered': discord.Colour(0xff4500), 'orchid': discord.Colour(0xda70d6),
        'palegoldenrod': discord.Colour(0xeee8aa), 'palegreen': discord.Colour(0x98fb98),
        'paleturquoise': discord.Colour(0xafeeee), 'palevioletred': discord.Colour(0xdb7093),
        'papayawhip': discord.Colour(0xffefd5), 'peachpuff': discord.Colour(0xffdab9), 'peru': discord.Colour(0xcd853f),
        'pink': discord.Colour(0xffc0cb), 'plum': discord.Colour(0xdda0dd), 'powderblue': discord.Colour(0xb0e0e6),
        'purple': discord.Colour(0x800080), 'rebeccapurple': discord.Colour(0x663399), 'red': discord.Colour(0xff0000),
        'rosybrown': discord.Colour(0xbc8f8f), 'royalblue': discord.Colour(0x4169e1),
        'saddlebrown': discord.Colour(0x8b4513), 'salmon': discord.Colour(0xfa8072),
        'sandybrown': discord.Colour(0xf4a460), 'seagreen': discord.Colour(0x2e8b57),
        'seashell': discord.Colour(0xfff5ee), 'sienna': discord.Colour(0xa0522d), 'silver': discord.Colour(0xc0c0c0),
        'skyblue': discord.Colour(0x87ceeb), 'slateblue': discord.Colour(0x6a5acd),
        'slategray': discord.Colour(0x708090), 'slategrey': discord.Colour(0x708090), 'snow': discord.Colour(0xfffafa),
        'springgreen': discord.Colour(0x00ff7f), 'steelblue': discord.Colour(0x4682b4), 'tan': discord.Colour(0xd2b48c),
        'teal': discord.Colour(0x008080), 'thistle': discord.Colour(0xd8bfd8), 'tomato': discord.Colour(0xff6347),
        'turquoise': discord.Colour(0x40e0d0), 'violet': discord.Colour(0xee82ee), 'wheat': discord.Colour(0xf5deb3),
        'white': discord.Colour(0xffffff), 'whitesmoke': discord.Colour(0xf5f5f5), 'yellow': discord.Colour(0xffff00),
        'yellowgreen': discord.Colour(0x9acd32)}

    class WrongArgs(Exception):
        pass

    class Perm(Enum):
        ADMIN = 'administrators'
        OWNER = 'owner'

    Command = namedtuple('Command', ['func', 'aliases', 'cmd', 'desc', 'emoji', 'colour', 'emoji_url', 'embed',
                                     'shlex', 'perm', 'channels'])

    def __init__(self):
        super().__init__()
        self.owner = None
        self.owners_litu_channel = None
        self.blank_emoji = '▪'
        self.on_msg_cmd = self._init_on_msg_cmd()

    async def on_connect(self):
        print(f'connected as {self.user}')

    async def on_ready(self):
        self.owner = (await self.application_info()).owner

        self.owners_litu_channel = self.get_channel(513337460684750864)

        tmp = discord.utils.find(lambda e: e.name == 'litu_blank', self.emojis)
        if tmp is None:
            tmp = discord.utils.find(lambda e: e.name in ('blank', 'empty'), self.emojis)
        if tmp is not None:
            self.blank_emoji = tmp

        print('ready')

    async def on_message(self, msg):
        if msg.author == self.user or msg.content == '':
            return

        try:
            cmd = self._msg_to_cmd(msg)
        except ValueError:
            return

        if isinstance(msg.channel, discord.TextChannel):
            where = f'#{msg.channel.name}@{msg.guild.name}'
        elif isinstance(msg.channel, discord.DMChannel):
            where = 'DM'
        print(f"CMD IN {repr(cmd)} in {where} "
              f"by {msg.author.name}#{msg.author.discriminator} <{msg.id}>")

        async with msg.channel.typing():
            # noinspection PyBroadException
            try:
                command = self.on_msg_cmd[cmd[0]]
                args = shlex.split(cmd[1]) if command.shlex else cmd[1]
            except (KeyError, ValueError) as e:
                print(f"CMD SOFTERR {{\n\t{type(e).__name__}\n\t{e}\n}}")
                await msg.add_reaction(self.CMD_SOFTERR_EMOJI)
                return
            except Exception:
                print(f"CMD HARDERR {{{traceback.format_exc()}}}")
                await msg.add_reaction(self.CMD_HARDERR_EMOJI)
                return

            if not self._is_cmd_usable(command, msg.author, msg.channel):
                print("CMD SOFTERR {CmdNotUsable}")
                await msg.add_reaction(self.CMD_SOFTERR_EMOJI)
                return

            # noinspection PyBroadException
            try:
                if args is not None:
                    await command.func(msg, args)
            except self.WrongArgs as e:
                print(f"CMD SOFTERR {{\n\t{type(e).__name__}\n\t{e}\n}}")
                await msg.add_reaction(self.CMD_SOFTERR_EMOJI)
                return
            except discord.Forbidden:
                print(f"CMD PERMISSION HARDERR {{{traceback.format_exc()}}}")
                await msg.add_reaction(self.CMD_HARDERR_EMOJI)
                return
            except Exception:
                print(f"CMD HARDERR {{{traceback.format_exc()}}}")
                await msg.add_reaction(self.CMD_HARDERR_EMOJI)
                return

    def _is_cmd_usable(self, command: Command, member: discord.Member, channel: discord.abc.Messageable) -> bool:
        """Return if a command is usable for a context

        :param command: command to check
        :param member: member that triggered the command
        :return: is the command usable for the context
        """
        if type(channel) not in command.channels:
            return False

        if len(command.perm) == 0:
            return True
        if self.Perm.OWNER in command.perm and member == self.owner:
            return True
        if isinstance(channel, discord.TextChannel) and \
                self.Perm.ADMIN in command.perm and member.guild_permissions.administrator:
            return True

        return False

    def _msg_to_cmd(self, msg: discord.Message) -> (str, str):
        """If <msg>.content is recognizable as a command, returns (cmd, args)
        Else raise ValueError

        :param msg: message to test
        :return: 2-tuple with command name and arguments
        """
        if msg.content.startswith('#') or msg.content.startswith('//'):
            raise ValueError("not a comment (prefixed by '#')")

        args = msg.content.split()
        prefix = args[0].lower()

        possibilities = [self.user.mention, f'<@!{self.user.id}>',
                         self.user.name.lower(), f'@{self.user.name.lower()}']
        try:
            if msg.guild.me.nick is not None:
                possibilities.extend([msg.guild.me.nick.lower(), f'@{msg.guild.me.nick.lower()}'])
        except AttributeError:
            pass

        without_first = msg.content[len(args[0]):].lstrip()

        if prefix in possibilities:
            return args[1], without_first[len(args[1]):].lstrip()
        elif isinstance(msg.channel, discord.DMChannel) or\
                any([p in msg.channel.name.lower().split('-') for p in possibilities]):
            return args[0], without_first

        raise ValueError("not a command")

    async def on_msg_cmd_help(self, msg: discord.Message, args: List[str]) -> None:
        """Display the help

        :param msg: message that triggered the command
        :param args: arguments of the command
        """
        if len(args) > 1:
            raise self.WrongArgs(args)

        if len(args) == 0:
            try:
                color = msg.guild.me.colour
            except AttributeError:
                color = self.BASE_COLOR
            embed = discord.Embed(
                colour=color,
                description=f"Prefix your command with {self.user.name}'s name, their mention or their nick OR type "
                            f"it in a channel with one of those prefix in its name\n"
                            "If your in this kind of channel, prefix your message with `#` or `//` in order to prevent "
                            f"{self.user.name} from interpreting it\n"
                            '\n'
                            "Type `help <command>` to display the specific documentation of the command\n"
                            '\n'
                            "Most of the time, an argument is a text without any whitespace so if you want to include "
                            'one, feel free to surround your argument with `\'simple\'` or `"double"` quotes\n'
                            "If you need to a quote character in an argument, precede it with a `\`\n"
                            '\n'
                            f"If {self.user.name} react with {self.CMD_SOFTERR_EMOJI}, your command has an error\n"
                            f"If it's {self.CMD_HARDERR_EMOJI}, an internal error has occurred, please contact "
                            f"{self.owner.mention}")
            embed.set_author(name=f"{self.user.name.upper()}'S DOCUMENTATION", icon_url=self.user.avatar_url)
            embed.add_field(name='Examples:',
                            value=f"```\n@{self.user.name} help``````\n{self.user.name.lower()} help warning```"
                                  f"{self.blank_emoji}",
                            inline=False)

            for cmd in [v for k, v in self.on_msg_cmd.items()
                        if k not in v.aliases and self._is_cmd_usable(v, msg.author, msg.channel)]:
                embed.add_field(name=f"{cmd.emoji}  {cmd.cmd[0]}", value=cmd.desc, inline=True)

            await msg.channel.send(embed=embed)
            return

        try:
            await msg.channel.send(embed=self.on_msg_cmd[args[0]].embed)
            return
        except KeyError:
            pass

        raise self.WrongArgs(args)

    async def on_msg_cmd_info(self, msg: discord.Message, args: List[str]) -> None:
        """Display information about the bot

        :param msg: message that triggered the command
        :param args: arguments of the command
        """
        if len(args) != 0:
            raise self.WrongArgs(args)

        embed = discord.Embed(colour=self.on_msg_cmd['info'].colour,
                              description=f"I'm {self.user.name}, a bot created by {self.owner.mention}!")
        embed.set_author(name=f"{self.user.name.upper()}'S INFO", icon_url=self.user.avatar_url)

        embed.add_field(name='Processor', value=platform.processor(), inline=True)
        embed.add_field(name='System', value=platform.system(), inline=True)
        embed.add_field(name='Python version', value=platform.python_version(), inline=True)
        embed.add_field(name='Python implementation', value=platform.python_implementation(), inline=True)
        embed.add_field(name='discord.py version', value=discord.__version__)

        await msg.channel.send(embed=embed)

    async def on_msg_cmd_warning(self, msg: discord.Message, args: List[str]) -> None:
        """Send a message with a warning

        :param msg: message that triggered the command
        :param args: arguments of the command
        """
        if not isinstance(self.owners_litu_channel, discord.TextChannel):
            raise ValueError('self.owners_litu_channel not correct', self.owners_litu_channel)

        cur_pos = 0
        try:
            chn = self.get_channel(int(args[cur_pos]))
            if chn is None or isinstance(chn, discord.abc.PrivateChannel):
                raise self.WrongArgs(args)
            cur_pos += 1
        except ValueError:
            chn = msg.channel
        except IndexError:
            raise self.WrongArgs(args)

        if not isinstance(chn, discord.TextChannel):
            raise self.WrongArgs(args)

        try:
            message = args[cur_pos]
        except IndexError:
            raise self.WrongArgs(args)
        if message.startswith('t=') or message.startswith('s=') or message.startswith('w='):
            message = ''
        else:
            cur_pos += 1

        trigger = []
        spoiler = []
        warning = []
        for arg in args[cur_pos:]:
            if arg.startswith('t='):
                trigger.append(arg[2:])
            elif arg.startswith('s='):
                spoiler.append(arg[2:])
            elif arg.startswith('w='):
                warning.append(arg[2:])
            else:
                raise self.WrongArgs(args)

        if len(message) + len(msg.attachments) == 0 or len(trigger) + len(spoiler) + len(warning) == 0:
            raise self.WrongArgs(args)

        chn_author = chn.guild.get_member(msg.author.id)

        uploaded = msg.attachments
        if chn == msg.channel and len(msg.attachments) > 0:
                lst_files = []
                for a in msg.attachments:
                    a_bytes = io.BytesIO(requests.get(a.url).content)
                    lst_files.append(discord.File(a_bytes, filename=a.filename))
                uploaded = (await self.owners_litu_channel.send(files=lst_files)).attachments
                await msg.delete()
        embed = discord.Embed(colour=chn_author.colour,
                              description=f"""{chn_author.mention} sent a message with a warning!
React with an emoji to receive the original message""")
        embed.set_footer(text='litu_warning')

        name = chn_author.name
        try:
            if chn_author.nick:
                name = f'{chn_author.nick} (@{chn_author.name})'
        except AttributeError:
            pass
        embed.set_author(name=name, icon_url=chn_author.avatar_url)

        if len(trigger) > 0:
            embed.add_field(name="Trigger warnings", value='\n'.join(trigger), inline=True)
        if len(spoiler) > 0:
            embed.add_field(name="Spoiler warnings", value='\n'.join(spoiler), inline=True)
        if len(warning) > 0:
            embed.add_field(name="Warnings", value='\n'.join(warning), inline=True)

        encoded = json.dumps({'message': message,
                              'attachments': [{'url': a.url, 'is_image': a.height is not None} for a in uploaded]
                              }).encode('utf-8')
        im = io.BytesIO()
        b64rgba.encode_image(base64.b64encode(encoded), (128, 128)).save(im, 'png')
        im_sent = await self.owners_litu_channel.send(file=discord.File(im.getvalue(), 'tmp.png'))

        embed.set_thumbnail(url=im_sent.attachments[0].url)

        sent = await chn.send(embed=embed)
        if len(chn.guild.emojis) > 0:
            react = random.choice(chn.guild.emojis)
        else:
            react = '🤔'
        await sent.add_reaction(react)

    async def on_msg_cmd_quote(self, msg: discord.Message, args: List[str]) -> None:
        """Send a quote and a link referring to a message

        :param msg: message that triggered the command
        :param args: arguments of the command
        """
        if len(args) < 1 or len(args) > 3:
            raise self.WrongArgs(args)

        match = self.RE_MSG_URL.match(args[0])
        if len(args) == 1 and match is not None:
            arg_msg = match.group('msg')
            arg_chn = match.group('chn')
        else:
            arg_msg = args[0]
            try:
                arg_chn = args[1]
            except IndexError:
                arg_chn = None

        try:
            msg_id = int(arg_msg)
        except ValueError:
            raise self.WrongArgs(args)

        try:
            chn = msg.channel if arg_chn is None else self.get_channel(int(arg_chn))
        except ValueError:
            try:
                channels = self.get_guild(int(args[2])).channels
            except IndexError:
                channels = msg.guild.channels
            except ValueError:
                channels = [c for g in filter(lambda g: g.name.lower() == args[2].lower(), self.guilds) for c in
                            g.channels]
            chn = discord.utils.find(lambda c: c.name.lower() == args[1], channels)

        try:
            orig_msg = await chn.get_message(msg_id)
        except (discord.NotFound, AttributeError):
            raise self.WrongArgs(args)

        await msg.channel.send(embed=self._get_quote_embed(orig_msg))

    def _get_quote_embed(self, msg: discord.Message) -> discord.Embed:
        """Return an Embed representing a quote of the message
        :param msg: message to quote
        :return: embed representing the quote
        """
        embed = discord.Embed(colour=msg.author.colour, description=msg.content,
                              timestamp=msg.created_at)

        im = discord.utils.find(lambda a: a.height is not None, msg.attachments)
        if im is not None:
            embed.set_thumbnail(url=im.url)

        name = msg.author.name
        try:
            if msg.author.nick:
                name = f'{msg.author.nick} (@{msg.author.name})'
        except AttributeError:
            pass
        embed.set_author(name=name, icon_url=msg.author.avatar_url)
        embed.set_footer(text=f'#{msg.channel.name}', icon_url=msg.guild.icon_url)

        embed.add_field(
            name=self.blank_emoji,
            value=f'[Link to the message](https://discordapp.com/channels/{msg.channel.guild.id}/'
                  f'{msg.channel.id}/{msg.id})')

        return embed

    async def on_msg_cmd_color(self, msg: discord.Message, args: str) -> None:
        """Change sender's color

        :param msg: message that triggered the command
        :param args: arguments of the command
        """
        name = re.sub(r'[^a-z]', '', args, flags=re.IGNORECASE).lower()

        role_name = f'{self.user.name.lower()}_color_{msg.author.id}'
        role = discord.utils.get(msg.guild.roles, name=role_name)

        try:
            color = self.DISCORD_COLORS[name]
        except KeyError:
            try:
                color = self.CSS3_COLORS[name]
            except KeyError:
                try:
                    if args[0] == '#':
                        color = discord.Colour(int(args[1:], 16))
                    else:
                        color = discord.Colour(int(args, 16))
                except ValueError:
                    try:
                        if name == 'none':
                            await role.delete()
                            return
                    except AttributeError:
                        raise self.WrongArgs(args)
                    raise self.WrongArgs(args)

        pos = msg.guild.me.top_role.position
        if role is not None:
            await role.edit(colour=color, position=pos - 1)
        else:
            role = await msg.guild.create_role(name=role_name, colour=color)
            await role.edit(position=pos)
        await msg.author.add_roles(role)

        if msg.author == self.owner:
            await msg.guild.me.add_roles(role)

    async def on_msg_cmd_colorfix(self, msg: discord.Message, args: str) -> None:
        """Delete the unused roles created with the command 'color'

        :param msg: message that triggered the command
        :param args: arguments of the command
        """
        if len(args) > 1:
            raise self.WrongArgs(args)

        if len(args) == 0:
            guilds = [msg.guild]
        else:
            if args[0] == 'all':
                guilds = self.guilds
            else:
                raise self.WrongArgs(args)

        for guild in guilds:
            for role in filter(lambda r: r.name.startswith(f'{self.user.name.lower()}_color_'), guild.roles):
                usr_id = int(role.name.split('_')[-1])
                if guild.get_member(usr_id) not in role.members:
                    await role.delete()

    async def on_msg_cmd_ulule(self, msg: discord.Message, args: str) -> None:
        """Display info about an Ulule project

        :param msg: message that triggered the command
        :param args: arguments of the command
        """
        pro = json.loads(requests.get(url=f'https://api.ulule.com/v1/projects/{args}').content)

        if 'message' in pro.keys():
            # noinspection PyUnresolvedReferences
            res = json.loads(requests.get(
                url=f"https://api.ulule.com/v1/search/projects?limit=1&q={urllib.parse.quote(args, safe='')}")
                             .content)
            pro = json.loads(requests.get(url=f"https://api.ulule.com/v1/projects/{res['projects'][0]['id']}").content)

        if 'message' in pro.keys():
            raise self.WrongArgs(args)

        percent = ''.join([f':{self.DIGIT_TO_WORD[int(d)]}:' if d.isdigit() else d for d in
                           '{:,}'.format(pro['percent']).replace(',', ' ')])
        embed = discord.Embed(colour=self.on_msg_cmd['ulule'].colour,
                              description=f'{percent} **%**')

        embed.set_image(url=pro['image'])
        embed.set_thumbnail(url='https://d1yggf0lcx0ykm.cloudfront.net/site/identity/logos/owl-blue.dbf445310290.png')
        embed.set_author(name=pro[f"name_{pro['lang']}"], url=pro['absolute_url'],
                         icon_url=pro['owner']['avatar']['full'])

        if pro['type'] == 1:
            embed.add_field(name=f"{'{:,}'.format(pro['committed']).replace(',', ' ')}",
                            value=f"presales on a goal of **{'{:,}'.format(pro['goal']).replace(',', ' ')}**",
                            inline=True)
        else:
            embed.add_field(name=f"{'{:,}'.format(pro['committed']).replace(',', ' ')} {pro['currency_display']}",
                            value=f"collected on a goal of **{'{:,}'.format(pro['goal']).replace(',', ' ')} "
                                  f"{pro['currency_display']}**",
                            inline=True)

        left_or_funded = pro['time_left']
        if left_or_funded is None:
            date = datetime.datetime.strptime(pro['date_end'], '%Y-%m-%dT%H:%M:%SZ').strftime('%d %B %Y').lower()
            left_or_funded = f"Funded on {date}"
        date_start = datetime.datetime.strptime(pro['date_start'], '%Y-%m-%dT%H:%M:%SZ').strftime('%d %B %Y').lower()
        embed.add_field(name=left_or_funded, value=f"Started on {date_start}", inline=True)

        await msg.channel.send(embed=embed)

    async def on_msg_cmd_emojif(self, msg: discord.Message, args: List[str]) -> None:
        """Send an animated emote of the server

        :param msg: message that triggered the command
        :param args: arguments of the command
        """
        if len(args) != 1:
            raise self.WrongArgs(args)

        e = discord.utils.get(msg.guild.emojis, name=args[0].strip(':'), animated=True)
        if e is None:
            raise self.WrongArgs(args)

        await msg.channel.send(e)

    # noinspection PyMethodMayBeStatic
    async def on_msg_cmd_exec(self, msg: discord.Message, args: str) -> None:
        """Execute a Python instruction

        :param msg: message that triggered the command
        :param args: arguments of the command
        """
        old_stdout = sys.stdout
        buffer = io.StringIO()
        sys.stdout = buffer

        # noinspection PyBroadException
        try:
            exec(args, globals(), locals())
        except Exception:
            print(f'```py\n{traceback.format_exc()}```')

        sys.stdout = old_stdout
        try:
            await msg.channel.send(buffer.getvalue())
        except discord.HTTPException:
            pass

        try:
            await msg.delete()
        except discord.Forbidden:
            pass

    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent) -> None:
        usr = self.get_user(payload.user_id)

        if usr == self.user:
            return

        chn = self.get_channel(payload.channel_id)
        msg = await chn.get_message(payload.message_id)

        print(f"REACT ADD {repr(payload.emoji)} in #{chn.name}@{chn.guild.name} "
              f"by {usr.name}#{usr.discriminator}")

        try:
            if msg.embeds[0].footer.text == 'litu_warning':
                await self.on_react_cmd_warning(msg, usr)
        except IndexError:
            pass

    @staticmethod
    async def on_react_cmd_warning(msg: discord.Message, usr: discord.User) -> None:
        """Send a DM with the original message behind behind the command "warning"

        :param msg: message concerned by the reaction
        :param usr: user that reacted
        """
        try:
            url = msg.embeds[0].thumbnail.url
        except LookupError:
            return

        im = Image.open(io.BytesIO(requests.get(url).content))
        decoded = json.loads(base64.b64decode(b64rgba.decode_image(im)).decode('utf-8'))

        embed = discord.Embed(colour=msg.embeds[0].colour, description=decoded['message'])
        embed.set_author(name=msg.embeds[0].author.name,
                         icon_url=msg.embeds[0].author.icon_url)
        embed.set_footer(text=f"posted on #{msg.channel.name} in {msg.guild.name}")

        if len(decoded['attachments']) > 0:
            res = discord.utils.find(lambda a: a['is_image'], decoded['attachments'])
            if res is not None:
                embed.set_image(url=res['url'])

            for i in range(math.ceil(len(decoded['attachments']) / 5)):
                embed.add_field(
                    name='Files attached',
                    value='\n'.join([f"[{a['url'].split('/')[-1]}]({a['url']})"
                                     for a in decoded['attachments'][5 * i:5 * (i + 1)]
                                     if i <= len(decoded['attachments'])]))

        await usr.send(embed=embed)

    # noinspection PyProtectedMember
    def _init_on_msg_cmd(self) -> OrderedDict:
        """Returns the initialization of self.on_msg_cmd

        :return: initialization of self.on_msg_cmd
        """
        odict = OrderedDict()

        embed = discord.Embed()
        odict['help'] = self.Command(
            self.on_msg_cmd_help, (), ('help [<command>]',),
            "Display this help",
            '📘', discord.Colour(0x55acee),
            'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/blue-book_1f4d8.png',
            embed, shlex=True, perm=(),
            channels=(discord.TextChannel, discord.DMChannel))
        embed.description = "Display the general help of the bot or a specific help for a command"
        embed.add_field(name='Examples', inline=False,
                        value="```c\nhelp``````c\nhelp warning```")

        embed = discord.Embed()
        odict['info'] = self.Command(
            self.on_msg_cmd_info, (), ('info',),
            "Display information about the bot",
            '📄', discord.Colour(0xccd6dd),
            'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/page-facing'
            '-up_1f4c4.png',
            embed, shlex=True, perm=(),
            channels=(discord.TextChannel, discord.DMChannel))
        embed.description = odict['info'].desc

        embed = discord.Embed()
        odict['warning'] = self.Command(
            self.on_msg_cmd_warning, ('trigger', 'spoiler'),
            ('warning [<channel_id>] [<message>] <warnings>',),
            "Send a message with a warning",
            '⚠', discord.Colour(0xffcc4d),
            'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/warning-sign_26a0.png',
            embed, shlex=True, perm=(),
            channels=(discord.TextChannel, discord.DMChannel))
        embed.description = f"""{odict['warning'].desc}
React with an emoji to receive the original message in private

You can send file, they will be linked in the private message with a preview of the first image attached
If the warning message is posted in the same channel where you write the command, this last message will be deleted

`<channel_id>` is the ID of a channel where you want to post the warning (default: current channel)
`<message>` is the original message that you want to hide (you don't need one if there is one or + attached files)
`<warnings>` is a list of warnings (minimum 1) starting with `t=`, `s=` or `w=` if it's respectively a """ \
                            """trigger warning, spoiler warning or general warning

If you want to send attached files with the original message where the warning message will be sent, you can, but """\
                            """be sure to know what's happening:
In order to be hidden, an image needs to have an valid URL. The problem is that when a message is deleted, all of """\
                            """its files' URL become invalid
This means that the bot needs to reupload the files and this will probably take a lot of time, a time during which """\
                            """your command message (and so the original message) will be visible to all
Consider providing URLs instead of files or triggering the command in a private message with the bot (using """\
                            """`<channel_id>`)

If you don't know how to find an ID, [click here](https://support.discordapp.com/hc/articles/206346498)"""
        embed.add_field(name='Examples', inline=False,
                        value="```c\ntrigger wow s=\"such spoiler\" t=\"many trigger\"```"
                              "```c\nwarning w='Knock Knock!'\n# with an image with a ghost attached```"
                              "```c\nspoiler 450509681967345139 \"Sasha is dead since the beginning\" s=Pokémon```"
                              "```c\ntrigger \"I'm a muffin!\nAnd it's muffin time!\\nWho wants a muffin?\\nPlease I "
                              "just wanna die!\" t=\"food death\" s=\"THE MUFFIN SONG (asdfmovie)\"```")

        embed = discord.Embed()
        odict['quote'] = self.Command(
            self.on_msg_cmd_quote, (),
            ('quote <message_id> [<channel> [<server>]]', 'quote <message_url>'),
            "Send a quote and a link referring to a message",
            '🔗', discord.Colour(0x8899a6),
            'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/link-symbol_1f517.png',
            embed, shlex=True, perm=(),
            channels=(discord.TextChannel,))
        embed.description = f"""{odict['quote'].desc}

`<message_id>` is the unique ID of a message
If the message is not in the current channel, you need to provide `<channel>` as a channel ID/name
If the channel is not in the current server, `<server>` is needed only if `<channel>` is a channel name (and not an ID)
`<message_url>` is a link to the message

The more information you provide, the easier and faster it is for the bot (but don't worry much about it)
If you don't know how to find an ID, [click here](https://support.discordapp.com/hc/articles/206346498)"""
        embed.add_field(name='Examples', inline=False,
                        value="```c\nquote 206119153541781924```"
                              "```c\nquote 171917611361543969 channel-in-this-server```"
                              '```c\nquote 615439690934323895 main-channel "Best Server!"```'
                              "```c\nquote 717367851719176116 450509681967345139```"
                              "```c\nquote https://discordapp.com/channels/430230244492991789/244191562727435452"
                              "/507069376942674169```")

        embed = discord.Embed()
        odict['color'] = self.Command(
            self.on_msg_cmd_color, ('colour',), ('color <name>', 'color <hex>', 'color none'),
            "Change sender's color",
            '🎨', discord.Colour(0xe6ab6e),
            'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/artist'
            '-palette_1f3a8.png',
            embed, shlex=False, perm=(),
            channels=(discord.TextChannel,))
        embed.description = f"""{odict['color'].desc}
        
Note that the bot will put the role with the color at the highest position possible but can't manage any role above """ \
                            f"""themself
If `none` is provided, it will delete your color

`<name>` can be a Discord color (see below) or a [CSS3 color](https://www.w3schools.com/cssref/css_colors.asp)
`<hex>` is an hexadecimal RGB value (prefixed or not by a `#`)

Discord colors are `{'`, `'.join(self.DISCORD_COLORS.keys())}`"""
        embed.add_field(name='Examples', inline=False,
                        value='```c\ncolor red```'
                              "```c\ncolour aliceblue```"
                              "```c\ncolor #8a2be2```"
                              "```c\ncolour none```")

        embed = discord.Embed()
        odict['colorfix'] = self.Command(
            self.on_msg_cmd_colorfix, (), ('colorfix [all]',),
            "Delete the unused roles created with the command `color`",
            '🖌️', discord.Colour(0x3889c4),
            'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/lower-left'
            '-paintbrush_1f58c.png',
            embed, shlex=True, perm=(self.Perm.OWNER,),
            channels=(discord.TextChannel,))
        embed.description = f"""{odict['colorfix'].desc}
Use the argument `all` to fix every server of the bot, else it will only be the current one"""
        embed.add_field(name='Examples', inline=False,
                        value='```c\ncolorfix```'
                              "```c\ncolorfix all```")

        embed = discord.Embed()
        odict['ulule'] = self.Command(
            self.on_msg_cmd_ulule, (), ('ulule <query>', 'ulule <project>'),
            "Display info about an Ulule project",
            '🦉', discord.Colour(0xc2694e),
            'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/owl_1f989.png',
            embed, shlex=False, perm=(),
            channels=(discord.TextChannel, discord.DMChannel))
        embed.description = f"""{odict['quote'].desc}

`<query>` is any sentence
`<project>` is a project's id or a project's [slug](https://en.wikipedia.org/wiki/Slug_(disambiguation\))"""
        embed.add_field(name='Examples', inline=False,
                        value="```c\nulule reflets acide jbx```"
                              "```c\nulule rdajdp``````c\nulule 75852```")

        embed = discord.Embed()
        odict['emojif'] = self.Command(
            self.on_msg_cmd_emojif, ('emogif',), ('emojif <emote>',),
            "Send an animated emote of the server",
            '✨', discord.Colour(0xaB8fd7),
            'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/160/twitter/31/sparkles_2728.png',
            embed, shlex=True, perm=(),
            channels=(discord.TextChannel,))
        embed.description = f"""{odict['quote'].desc}

`<emote>` is the name of an animated emote of the current server, surrounded or not by `:`"""
        embed.add_field(name='Examples', inline=False,
                        value="```c\nemojif smilegif```"
                              "```c\nemogif :nopcat:```")

        embed = discord.Embed()
        odict['exec'] = self.Command(
            self.on_msg_cmd_exec, (), ('exec <instruction>',),
            "Execute a Python instruction",
            '🐍', discord.Colour(0x77b255),
            'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/154/snake_1f40d.png',
            embed, shlex=False, perm=(self.Perm.OWNER,),
            channels=(discord.TextChannel, discord.DMChannel))
        embed.description = "Execute a Python instruction (or set of instructions) with exec()"
        embed.add_field(name='Examples', inline=False,
                        value="```c\nexec 1 + 3``````c\nexec\nfor i in range(10):\n   print(2 ** i)```")

        def channel_type_name(chn_t: type) -> str:
            if chn_t == discord.TextChannel:
                return 'server'
            elif chn_t == discord.DMChannel:
                return 'private'
            else:
                raise ValueError(f'channel type unexpected: {chn_t}')

        for key, command in odict.items():
            command.embed.colour = command.colour
            command.embed.set_author(name=f"{command.emoji} {key.upper()} DOCUMENTATION")
            command.embed.add_field(name='Syntax', inline=True,
                                    value=''.join(map(lambda x: f"```ini\n{x}```", command.cmd)))
            if len(command.aliases) > 0:
                command.embed.add_field(name='Aliases', inline=True,
                                        value=f"```\n{' '.join(command.aliases)}```")

            by = '`everyone`' if len(command.perm) == 0 else ' '.join([f'`{p.value}`' for p in command.perm])
            in_ = ' '.join([f'`{channel_type_name(c)}`' for c in command.channels])
            command.embed.add_field(name='Permissions', inline=True,
                                    value=f"Usable by {by} in {in_}")

            if command.embed._fields[0]['name'] == 'Examples':
                command.embed._fields.append(command.embed._fields.pop(0))

        for command in list(odict.values())[:]:
            for a in command.aliases:
                odict[a] = command

        return odict
